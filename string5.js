const string5 = data => {
    if(!(Array.isArray(data))){
        return "";

    }
    else{
        if(data.length == 0){
            return "";
        }
        else{
            return data.join(" ");
        }
    }
}

module.exports = string5;