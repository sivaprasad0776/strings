const string3 = st1 => {
    if (st1 == null || st1.length == 0 || typeof st1 !='string') {
        return 0;
    }
    else {
        // considering the date in dd/mm/yyyy format
        const re = /^(0?[1-9]|[12][0-9]|3[01])\/(0?[1-9]|1[12])\/(19|20)\d\d$/ ;
        if (!re.test(st1)) {
            return 0;
        }
        else {
            const ids = st1.split('/');
            
            return parseInt(ids[0]);
        }
    }

}

module.exports = string3;


