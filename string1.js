const string1 = st1 => {
    if (st1 == null || st1.length == 0 || typeof st1 !='string') {
        return 0;
    }
    else {
        const re = /^-?\$?((0|[1-9]\d{0,2}(,\d{3})*)|(0|[1-9]\d*))?(\.\d{1,2})?$/;
        if (!re.test(st1)) {
            return 0;
        }
        else {
            let result = st1.replace('$', '');
            result = result.replace(',', '');
            return result;
        }
    }

}

module.exports = string1;
