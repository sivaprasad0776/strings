const string2 = st1 => {
    if (st1 == null || st1.length == 0 ||  typeof st1 !='string') {
        return [];
    }
    else {
        const re = /^((0{1,3}|0{0,2}\d|0{0,1}[1-9][0-9]|1\d\d|2[0-4]\d|25[0-5])\.){3}(0{1,3}|0{0,2}\d|0{0,1}[1-9][0-9]|1\d\d|2[0-4]\d|25[0-5])$/;
        if (!re.test(st1)) {
            return [];
        }
        else {
            const ids = st1.split('.');
            const intIds = [];
            for(let i = 0; i < ids.length ; i++){
                intIds.push(parseInt(ids[i]));
            }
            return intIds;
        }
    }

}

module.exports = string2;


