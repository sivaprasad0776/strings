const string4 = personName => {
    if(! (personName && typeof personName === 'object' && personName.constructor === Object) ){
        return '';
    }
    else {
        let result = "" ;
        ({first_name="", middle_name="", last_name=""} = personName);  
        if(first_name != ""){
            first_name = first_name.charAt(0).toUpperCase() + first_name.slice(1).toLowerCase();
            result += first_name;
        }
        if(middle_name != ""){
            middle_name = middle_name.charAt(0).toUpperCase() + middle_name.slice(1).toLowerCase();
            result +=  " " + middle_name;
        }
        if(last_name != ""){
            last_name = last_name.charAt(0).toUpperCase() + last_name.slice(1).toLowerCase();
            result +=  " " + last_name;
        }
        return  result;
       
    }

}

module.exports = string4;